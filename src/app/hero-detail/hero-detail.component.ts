import { Component, Input } from "@angular/core";
import { Hero } from '../hero/hero';

// Defines metadata for Component
@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})

export class HeroDetailComponent {
  @Input() hero: Hero;
}
